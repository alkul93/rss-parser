### Требования к системе
- Windows/Linux/MacOS
- docker 20.10.6+
- docker-compose 1.29.1+

### Как развернуть проект

1. Склонировать репозиторий (`git clone адрес проекта`)
2. Создать файл `project_path/.env` на основе файла `project_path/.env.example`
3. (*опционально) Забиндить локальный адрес localhost с указанием порта (IP: 127.0.0.1, PORT: указан в .env файле,
   в константе DOCKER_WEBSERVER_EXTERNAL_PORT) на обычный url （напр. my-new-project.local）
4. В корне проекта запустить команду `docker-compose up -d`
5. Внутри контейнера php (название контейнера формируется из константы DOCKER_CONTAINER_NAME-название контейнера,
   напр. rp-php) выполнить команды для инициализации проекта, пример:
    - **docker exec -it rp-php bash**
    - **composer install**
    - **php artisan key:generate**
    - **php artisan storage:link**
   
### Комментарии разработчика
* Вместо запуска консольной команды по крону или в режиме демона, при старте контейнера запускается команда
`php artisan schedule:work` соотвественно она и выполняет роль планировщика. 
* Команда для получение данных из rss: `php artisan parse:rbk-rss`. Команда поддерживает два опциональных 
параметра:
   - `--update-news` - новости обновятся, если изменятся на стороне рбк
   - `--show-output` - вывод информации при выполенении команды (начало, прогресс выполенния, завершение и т.п.)
* Команда `php artisan parse:rbk-rss` на текущий момент запускается раз в минуту, соотвественно после запуска
контейнера, в течении минуты можно увидеть данные в бд.
* Эндпоинт откуда получать данные вынесен в .env
  

* Не стал писать отдельные сервисы и т.п. ибо не вижу причин перегружать абстракциями простую задачу
* Не стал добавлять админку, т.к. про то как она должна выглядеть в ТЗ ничего не сказано 
  (нужна авторизация/не нужна, какие страницы, какие поля и т.п.). Если сильно нужно, в качестве 
  быстрого решения, можно заюзать laravel nova или laravel invoker.
* Не стал писать инструкцию по разворачиванию без докера, ибо там все стандартно:
   - Поднять php 7.4/8 версии (предпочтительно 8.0.6, в 8.0.4/8.0.5 есть проблемы с pdo, 
     не выполнятся миграции)
   - Поднять postgresql 12.7+ версии
   - Поднять nginx, заставить смотреть в project_path/public
   - Прописать доступы в .env
   - Запустить команду `php artisan schedule:work` как сервис линуха или добавить в crontab запись
   `* * * * * /usr/local/bin/php project_path/artisan schedule:run >> /dev/null 2>&1`


* Время выполнения: 1.5 часа, включая настройку докера и разворачивание проекта

### Используемые технологии

- php 8.0.6
- postgresql 12.7
- nodejs 14.x
- mysql 8
- redis as cache
- laravel framework 8.4x
- vuejs 2.x
- docker 20.10.x
- docker-compose 1.29.x