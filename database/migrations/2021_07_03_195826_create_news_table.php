<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('guid')->unique();
            $table->string('name');
            $table->string('link');
            $table->text('description');
            $table->dateTime('publish_date');
            $table->string('author')->nullable();
            $table->string('image')->nullable();

            $table->index('guid');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news');
    }
}