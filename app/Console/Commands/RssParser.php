<?php

namespace App\Console\Commands;

use App\Models\Log;
use App\Models\News;
use DateTime;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class RssParser extends Command
{
    protected $signature = 'parse:rbk-rss {--update-news} {--show-output}';

    protected $description = 'Parsing rss from rbk';

    /**
     * @throws Exception
     */
    public function handle()
    {
        $endpoint = config('services.rss.rbk_endpoint');

        $log = new Log([
            'request_time' => new DateTime,
            'request_method' => Log::METHOD_GET,
            'request_url' => $endpoint,
        ]);

        if ($this->option('show-output')) {
            $this->info("Getting news.");
            $this->info("Try request to '$endpoint'...");
        }

        $response = Http::get($endpoint);

        $log->response_http_code = $response->status();
        $log->response_body = $response->body();
        $log->save();

        if (!$response->successful()) {
            throw new Exception("Receiving rss data unsuccessfully. Response code is {$response->status()}");
        }

        $result = (new XmlEncoder())->decode($response->body(), 'array');

        $receivedNews = collect($result['channel']['item'])
            ->map(fn($item) => [
                'guid' => $item['guid'],
                'name' => $item['title'],
                'link' => $item['link'],
                'description' => $item['description'],
                'publish_date' => $item['pubDate'],
                'author' => $item['author'] ?? null,
                'image' => ($item['enclosure']['@url'] ?? false) ? $item['enclosure']['@url'] : $item['enclosure'][0]['@url'] ?? null,
            ]);

        if ($this->option('show-output')) {
            $this->info("Received news count: {$receivedNews->count()}");
        }

        $bar = $this->output->createProgressBar($receivedNews->count());

        foreach ($receivedNews as $item) {
            $news = News::where('guid', $item['guid'])->first() ?? new News($item);

            if ($this->option('update-news')) {
                $news->fill($item);
            }

            $news->save();

            if ($this->option('show-output')) {
                $bar->advance();
            }
        }

        if ($this->option('show-output')) {
            $bar->finish();
            $this->info("\ndone.");
        }

    }
}
