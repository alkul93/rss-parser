<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Log
 * @package App\Models
 *
 * @property string $guid
 * @property string $name
 * @property string $link
 * @property string $description
 * @property Carbon $publish_date
 * @property string $author
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class News extends Model
{
    protected $fillable = [
        'guid',
        'name',
        'link',
        'description',
        'publish_date',
        'author',
        'image'
    ];

    protected $dates = ['created_at', 'updated_at', 'publish_date'];
}