<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Log
 * @package App\Models
 *
 * @property Carbon $request_time
 * @property string $request_method
 * @property string $request_url
 * @property string $response_http_code
 * @property string $response_body
 *
 */
class Log extends Model
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';

    protected $fillable = [
        'request_time',
        'request_method',
        'request_url',
        'response_http_code',
        'response_body',
    ];
}